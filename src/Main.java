import frontend.GlavniProzor;
import javafx.application.Application;

public class Main {

    public static void main(String args[])  {
      Application.launch(GlavniProzor.class);
    }
}