package frontend;

import backend.BazaPodataka;
import backend.Unos;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;

import java.util.ArrayList;

public class Tabela {

    private GridPane kontejnerZaTabelu;
    private ArrayList<Unos> ulancanaLista;
    private TableView tabela;
    BazaPodataka BazaPodataka;

    /**
     *  konstruktor za Tabelarni prikaz.
     */
    public Tabela(ArrayList<Unos> ulancanaLista, BazaPodataka BazaPodataka) {
        kontejnerZaTabelu = new GridPane();
        this.ulancanaLista = ulancanaLista;
        this.BazaPodataka = BazaPodataka;
    }

    /**
     * metoda koja ce popuniti tabelu i konstantno proveravati
     * njen redosled i da li je nov unos dodat u listu.
     */
    public void popuniTabele(ObservableList<Unos> unos) {

        tabela = new TableView();
        tabela.getSelectionModel().setCellSelectionEnabled(true);

        // kako ce izgledati tabelarni prikaz.
        TableColumn<String, String> imeKolona = new TableColumn<>("Ime");
        TableColumn<String, String> prezimaKolona = new TableColumn<>("Prezime");
        TableColumn<String, String> brojTelefonaKolona = new TableColumn<>("Broj Telefona");
        TableColumn<String, String> adresaKolona = new TableColumn<>("Adresa");
        TableColumn<String, String> gradKolona = new TableColumn<>("Grad");

        ///minimalna sirina kolone
        imeKolona.setMinWidth(125);
        prezimaKolona.setMinWidth(125);
        brojTelefonaKolona.setMinWidth(125);
        adresaKolona.setMinWidth(125);
        gradKolona.setMinWidth(125);

        // boja pozadine kolona
        imeKolona.setStyle("-fx-font: 22 arial; -fx-base: #67494f;");
        prezimaKolona.setStyle("-fx-font: 22 arial; -fx-base: #67494f;");
        brojTelefonaKolona.setStyle("-fx-font: 22 arial; -fx-base: #67494f;");
        adresaKolona.setStyle("-fx-font: 22 arial; -fx-base: #67494f;");
        gradKolona.setStyle("-fx-font: 22 arial; -fx-base: #67494f;");


        // nazivi kolona
        imeKolona.setCellValueFactory(new PropertyValueFactory<>("ime"));
        prezimaKolona.setCellValueFactory(new PropertyValueFactory<>("prezime"));
        brojTelefonaKolona.setCellValueFactory(new PropertyValueFactory<>("brojTelefona"));
        adresaKolona.setCellValueFactory(new PropertyValueFactory<>("adresa"));
        gradKolona.setCellValueFactory(new PropertyValueFactory<>("nazivGrada"));

        //dodavanje kolona u kontejner
        tabela = null;
        tabela = new TableView<>();
        tabela.setItems(null);
        tabela.setItems(unos);
        tabela.getColumns().addAll(imeKolona, prezimaKolona, brojTelefonaKolona, adresaKolona, gradKolona);
        kontejnerZaTabelu.setPadding(new Insets(10, 10, 10, 10));
        kontejnerZaTabelu.setVgap(10);
        kontejnerZaTabelu.setHgap(10);
        GridPane.setConstraints(tabela, 4, 4);
        kontejnerZaTabelu.getChildren().addAll(tabela);
    }

    /**
     * ova metoda ce povuci sve Unose iz baze podataka.
     */
    public void dajUnoseIzBaze() {
        ObservableList<Unos> list = null;
            list = BazaPodataka.vratiUnose();
            popuniTabele(list);

            for (int i = 0; i < list.size(); i++) {
                ulancanaLista.add(list.get(i));
            }
        }

    /**
     * Ova metoda ce proslediti sve unose iz baze podataka u tabelu.
     */
    public void uzmiUnose() {

        ObservableList<Unos> unos = FXCollections.observableArrayList();

        for (int i = 0; i < ulancanaLista.size(); i++) {
            Unos e = ulancanaLista.get(i);
            unos.add(new Unos(e.getId(), e.getIme(), e.getPrezime(), e.getBrojTelefona(), e.getAdresa(), e.getNazivGrada()));
        }
        this.popuniTabele(unos);
    }


    /**
     * Ova metoda omogucava korisniku da filtrira po
     * prezimenu kada klikne na dume "filtriraj".
     * Ali kada opet klikne, razultati ce sve vratiti na staro.

     * @param recZaFiltriranje prezime koje ce biti filtrirano.
     */
    public void filter(String recZaFiltriranje, boolean isTrue) {
        ArrayList<Unos> lista = new ArrayList<>();
        ObservableList<Unos> unos = FXCollections.observableArrayList();

        if (isTrue == true) {
            for (int i = 0; i < ulancanaLista.size(); i++) {
                if ((ulancanaLista.get(i).getPrezime().matches("(.*)" + recZaFiltriranje + "(.*)"))) {
                    lista.add(ulancanaLista.get(i));
                    unos.add(new Unos(ulancanaLista.get(i).getId(), ulancanaLista.get(i).getIme(), ulancanaLista.get(i).getPrezime(),
                            ulancanaLista.get(i).getBrojTelefona(), ulancanaLista.get(i).getAdresa(), ulancanaLista.get(i).getNazivGrada()));
                }
            }
            this.popuniTabele(unos);
        } else if (isTrue == false) {

            for (int i = 0; i < ulancanaLista.size(); i++) {
                Unos e = ulancanaLista.get(i);
                unos.add(new Unos(e.getId(), e.getIme(), e.getPrezime(), e.getBrojTelefona(), e.getAdresa(), e.getNazivGrada()));
            }
            this.popuniTabele(unos);
        }
    }

    public GridPane getKontejnerZaTabelu() {
        return kontejnerZaTabelu;
    }

    public TableView getTabela() {
        return tabela;
    }
}