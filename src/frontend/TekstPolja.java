package frontend;

import javafx.geometry.Insets;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class TekstPolja {
    private TextField imeTekstPolje;
    private TextField prezimeTekstPolje;
    private TextField brojTelefonaTekstPolje;
    private TextField adresaTekstPolje;
    private TextField nazivGradaTekstPolje;

    private GridPane kontejnerZaTekstPolja = new GridPane();

    /**
     * Konstruktor za tekstualna polja.
     */
    public TekstPolja() {

        //instanciranje tekstualnih polja.
        this.imeTekstPolje = new TextField();
        this.prezimeTekstPolje = new TextField();
        this.brojTelefonaTekstPolje = new TextField();
        this.adresaTekstPolje = new TextField();
        this.nazivGradaTekstPolje = new TextField();

        //postavljanje teksta koji ce se prikazivati.
        this.imeTekstPolje.setPromptText("Unesite ime");
        this.prezimeTekstPolje.setPromptText("Unesite prezime");
        this.brojTelefonaTekstPolje.setPromptText("Unesite broj telefona");
        this.adresaTekstPolje.setPromptText("Unesite adresu");
        this.nazivGradaTekstPolje.setPromptText("Unesite grad");

        //pozicioniranje tekstualnih polja.
        GridPane.setConstraints(this.imeTekstPolje, 0, 7);
        GridPane.setConstraints(this.prezimeTekstPolje, 4, 7);
        GridPane.setConstraints(this.brojTelefonaTekstPolje, 6, 7);
        GridPane.setConstraints(this.adresaTekstPolje, 0, 10);
        GridPane.setConstraints(this.nazivGradaTekstPolje, 4, 10);


        // postavljanje minimalne velicine tekstualnih polja.
        this.imeTekstPolje.setMinSize(30, 40);
        this.prezimeTekstPolje.setMinSize(30, 40);
        this.brojTelefonaTekstPolje.setMinSize(30, 40);
        this.adresaTekstPolje.setMinSize(30, 40);
        this.nazivGradaTekstPolje.setMinSize(30, 40);

        kontejnerZaTekstPolja.setPadding(new Insets(10, 10, 10, 10));
        kontejnerZaTekstPolja.setVgap(10);
        kontejnerZaTekstPolja.setHgap(10);

        //dodavanje tekstualnih polja u kontejner.
        kontejnerZaTekstPolja.getChildren().addAll(imeTekstPolje, prezimeTekstPolje, brojTelefonaTekstPolje, adresaTekstPolje, nazivGradaTekstPolje);
    }

    public TextField getImeTekstPolje() {
        return imeTekstPolje;
    }

    public TextField getPrezimeTekstPolje() {
        return prezimeTekstPolje;
    }

    public TextField getBrojTelefonaTekstPolje() {
        return brojTelefonaTekstPolje;
    }

    public GridPane getKontejnerZaTekstPolja() {
        return kontejnerZaTekstPolja;
    }

    public TextField getAdresaTekstPolje() {
        return adresaTekstPolje;
    }

    public TextField getNazivGradaTekstPolje() {
        return nazivGradaTekstPolje;
    }

}