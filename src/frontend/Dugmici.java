package frontend;

import backend.Imenik;
import backend.NapraviXML;
import backend.Unos;
import backend.Stampac;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.GridPane;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.sql.SQLException;

public class Dugmici implements EventHandler<ActionEvent> {

    private TekstPolja polja;
    private Button dugmeUnesi;
    private Button dugmePromeni;
    private Button dugmeIzbrisi;
    private Button dugmeStampaj;
    private Button dugmeIzadji;
    private ToggleButton dugmeFiltriraj;
    private GridPane kontejnerZaDugme = new GridPane();
    private Imenik imenik;
    private Tabela tabela;

    /**
     * Konstruktor za dugmice.
     */
    public Dugmici(TekstPolja polja, Imenik imenik, Tabela tabela) {

        this.imenik = imenik;
        this.polja = polja;
        this.tabela = tabela;

        //inicijalizacija i dodavanje teksta.
        dugmeUnesi = new Button("Unesi");
        dugmePromeni = new Button("Promeni");
        dugmeIzbrisi = new Button("Izbrisi");
        dugmeStampaj = new Button("Stampaj");
        dugmeIzadji = new Button("Izadji");
        dugmeFiltriraj = new ToggleButton("Filter");

        //postavljamo akcije za dugmice(program ce potraziti metodu "handle" u ovoj klasi)
        dugmeUnesi.setOnAction(this);
        dugmePromeni.setOnAction(this);
        dugmeIzbrisi.setOnAction(this);
        dugmeStampaj.setOnAction(this);
        dugmeIzadji.setOnAction(this);
        dugmeFiltriraj.setOnAction(this);

        //pozicioniranje dugmica.
        GridPane.setConstraints(dugmeUnesi, 1, 0);
        GridPane.setConstraints(dugmePromeni, 5, 0);
        GridPane.setConstraints(dugmeIzbrisi, 9, 0);
        GridPane.setConstraints(dugmeStampaj, 12, 0);
        GridPane.setConstraints(dugmeFiltriraj, 15, 0);
        GridPane.setConstraints(dugmeIzadji, 18, 0);

        //davanje odredjene boje dugmicima
        dugmeUnesi.setStyle("-fx-font: 22 arial; -fx-base: #0f9b29;");
        dugmePromeni.setStyle("-fx-font: 22 arial; -fx-base: #724011;");
        dugmeIzbrisi.setStyle("-fx-font: 22 arial; -fx-base: #86a50a;");
        dugmeStampaj.setStyle("-fx-font: 22 arial; -fx-base: #0063ac;");
        dugmeIzadji.setStyle("-fx-font: 22 arial; -fx-base: #761214;");
        dugmeFiltriraj.setStyle("-fx-font: 22 arial; -fx-base: #405054;");

        kontejnerZaDugme.setPadding(new Insets(10, 10, 10, 10));
        kontejnerZaDugme.setVgap(10);
        kontejnerZaDugme.setHgap(10);
        //postavljanje dugmica u kontejner
        kontejnerZaDugme.getChildren().addAll(this.dugmeUnesi, this.dugmePromeni, this.dugmeIzbrisi,
                this.dugmeStampaj, this.dugmeIzadji, dugmeFiltriraj);
    }

    /**
     * ova metoda se poziva kada se klikne na neko dugme.
     */
    @Override
    public void handle(ActionEvent event) {

        //ukoliko smo kliknuli na dugme "izadji"
        if (event.getSource().equals(dugmeIzadji)) {
            NapraviXML napraviXML = new NapraviXML();
            try {
                napraviXML.napraviXmlDokument(imenik.getLista());
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (TransformerException e) {
                e.printStackTrace();
            }
            System.exit(0);

            //ukoliko smo kliknuli na dugme "unesi"
        } else if (event.getSource().equals(dugmeUnesi)) {
            if (polja.getImeTekstPolje().getText() != null && polja.getImeTekstPolje().getText().isEmpty() &&
                    polja.getImeTekstPolje().getText().equals("")) {
                return;
            } else if (polja.getPrezimeTekstPolje().getText() != null && polja.getPrezimeTekstPolje().getText().isEmpty() &&
                    polja.getPrezimeTekstPolje().getText().equals("")) {
                return;
            } else if (polja.getBrojTelefonaTekstPolje().getText() != null && polja.getBrojTelefonaTekstPolje().getText().isEmpty() && polja.getBrojTelefonaTekstPolje().getText().equals("")) {
                return;
            } else if (polja.getAdresaTekstPolje().getText() != null && polja.getAdresaTekstPolje().getText().isEmpty() &&
                    polja.getAdresaTekstPolje().getText().equals("")) {
                return;
            } else if (polja.getNazivGradaTekstPolje().getText() != null && polja.getNazivGradaTekstPolje().getText().isEmpty() &&
                    polja.getNazivGradaTekstPolje().getText().equals("")) {
                return;
            } else {
                String ime = polja.getImeTekstPolje().getText().toLowerCase();
                String prezime = polja.getPrezimeTekstPolje().getText().toLowerCase();
                String brojTelefona = polja.getBrojTelefonaTekstPolje().getText().toLowerCase();
                String adresa = polja.getAdresaTekstPolje().getText().toLowerCase();
                String nazivGrada = polja.getNazivGradaTekstPolje().getText().toLowerCase();
                try {
                    imenik.dodajUnos(ime, prezime, brojTelefona, adresa, nazivGrada);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                polja.getImeTekstPolje().clear();
                polja.getPrezimeTekstPolje().clear();
                polja.getBrojTelefonaTekstPolje().clear();
                polja.getAdresaTekstPolje().clear();
                polja.getNazivGradaTekstPolje().clear();
            }

            //ukoliko smo kliknuli na dugme "izbrisi"
        } else if (event.getSource().equals(dugmeIzbrisi)) {
            ObservableList<Unos> listaTabela = tabela.getTabela().getSelectionModel().getSelectedItems();
            Unos unos = listaTabela.get(0);
            imenik.izbrisiUnos(unos.getId());

            //ukoliko smo kliknuli na dugme "promeni"
        } else if (event.getSource().equals(dugmePromeni)) {
            ObservableList<Unos> litaTabela = tabela.getTabela().getSelectionModel().getSelectedItems();
            int id = litaTabela.get(0).getId();
            String ime, prezime, brojTelefona, adresa, nazivGrada;

            if (polja.getImeTekstPolje().getText() != null && polja.getImeTekstPolje().getText().isEmpty() &&
                    polja.getImeTekstPolje().getText().equals("")) {
                ime = litaTabela.get(0).getIme();
            } else {
                ime = polja.getImeTekstPolje().getText();
            }
            if (polja.getPrezimeTekstPolje().getText() != null && polja.getPrezimeTekstPolje().getText().isEmpty() &&
                    polja.getPrezimeTekstPolje().getText().equals("")) {
                prezime = litaTabela.get(0).getPrezime();
            } else {
                prezime = polja.getPrezimeTekstPolje().getText();
            }
            if (polja.getBrojTelefonaTekstPolje().getText() != null && polja.getBrojTelefonaTekstPolje().getText().isEmpty() && polja.getBrojTelefonaTekstPolje().getText().equals("")) {
                brojTelefona = litaTabela.get(0).getBrojTelefona();
            } else {
                brojTelefona = polja.getBrojTelefonaTekstPolje().getText();
            }
            if (polja.getAdresaTekstPolje().getText() != null && polja.getAdresaTekstPolje().getText().isEmpty() &&
                    polja.getAdresaTekstPolje().getText().equals("")) {
                adresa = litaTabela.get(0).getAdresa();
            } else {
                adresa = polja.getAdresaTekstPolje().getText();
            }
            if (polja.getNazivGradaTekstPolje().getText() != null && polja.getNazivGradaTekstPolje().getText().isEmpty() &&
                    polja.getNazivGradaTekstPolje().getText().equals("")) {
                nazivGrada = litaTabela.get(0).getNazivGrada();
            } else {
                nazivGrada = polja.getNazivGradaTekstPolje().getText();
            }
            imenik.promeniUnos(id, ime.toLowerCase(), prezime.toLowerCase(), brojTelefona.toLowerCase(), adresa.toLowerCase(), nazivGrada.toLowerCase());
            polja.getImeTekstPolje().clear();
            polja.getPrezimeTekstPolje().clear();
            polja.getBrojTelefonaTekstPolje().clear();
            polja.getAdresaTekstPolje().clear();
            polja.getNazivGradaTekstPolje().clear();

            //ukoliko smo kliknuli na dugme "stampaj"
        } else if (event.getSource().equals(dugmeStampaj)) {
            Stampac Stampac = new Stampac(imenik.getLista());
            Stampac.actionPerformed();

            //ukoliko smo kliknuli na dugme "filtriraj"
        } else if (event.getSource().equals(dugmeFiltriraj)) {
            if (dugmeFiltriraj.isSelected()) {
                if (polja.getPrezimeTekstPolje().getText() != null && polja.getPrezimeTekstPolje().getText().isEmpty() &&
                        polja.getPrezimeTekstPolje().getText().equals("")) {
                    return;
                } else {
                    tabela.filter(polja.getPrezimeTekstPolje().getText(), true);
                }
            }
            if (!dugmeFiltriraj.isSelected()) {
                tabela.filter(" ", false);
            }
        }
    }

    public GridPane getKontejnerZaDugme() {
        return kontejnerZaDugme;
    }

}