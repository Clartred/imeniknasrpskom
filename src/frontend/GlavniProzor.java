package frontend;

import backend.BazaPodataka;
import backend.Imenik;
import backend.Unos;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.util.ArrayList;

public class GlavniProzor extends Application {

    /**
     * ovo je pocetna metoda Jave FX-a.
     */
    @Override
    public void start(Stage primaryStage)  {
        //Instanciranje svih potrebnih objekata.
        BazaPodataka BazaPodataka = new BazaPodataka();
        BazaPodataka.proveriDaLiBazaPostoji();
        ArrayList<Unos> list = new ArrayList<>();
        Tabela tabela = new Tabela(list, BazaPodataka);
        tabela.dajUnoseIzBaze();
        Imenik imenik = new Imenik(BazaPodataka, list, tabela);
        TekstPolja tekstualnaPolja = new TekstPolja();
        Dugmici dugmici = new Dugmici(tekstualnaPolja, imenik, tabela);
        BorderPane glavniKontejner = new BorderPane();

        //postavljanje manjih kontejnera u glavni kontejner
        glavniKontejner.setTop(dugmici.getKontejnerZaDugme());
        glavniKontejner.setBottom(tekstualnaPolja.getKontejnerZaTekstPolja());
        glavniKontejner.setCenter(tabela.getKontejnerZaTabelu());

        //postavljanje boje glavnog kontejnera.
        glavniKontejner.setStyle("-fx-font: 22 arial; -fx-base: #609bff;");
        Scene mainScene = new Scene(glavniKontejner, 850, 700);
        primaryStage.setTitle("Imenik");
        primaryStage.setScene(mainScene);
        primaryStage.show();
    }
}