package backend;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class NapraviXML {
    /**
     *  Ova metoda koristi podatke iz array liste i pravi xml eksport iz njih.
     * @param lista array lista koja ce se koristiti za pravljenje xml-a.
     */
    public void napraviXmlDokument(ArrayList<Unos> lista) throws ParserConfigurationException, TransformerException {

        DocumentBuilderFactory fabrikaZaDokument = DocumentBuilderFactory.newInstance();
        DocumentBuilder graditeljDokumenta = fabrikaZaDokument.newDocumentBuilder();
        //pravimo dokument
        Document dokument = graditeljDokumenta.newDocument();
        Element element = dokument.createElement("Imenik");
        dokument.appendChild(element);
       //ovde dodajemo elemente u xml
        for (int i = 0; i < lista.size(); i++) {

            Unos unos = lista.get(i);
            String idStringa = Integer.toString(unos.getId());
            Element id = dokument.createElement("ID");
            Element ime = dokument.createElement("ime");
            Element prezime = dokument.createElement("prezime");
            Element brojTelefona = dokument.createElement("brojTelefona");
            Element adresa = dokument.createElement("adresa");
            Element nazivGrada = dokument.createElement("nazivGrada");
            id.appendChild(dokument.createTextNode((idStringa)));
            ime.appendChild(dokument.createTextNode(unos.getIme()));
            prezime.appendChild(dokument.createTextNode(unos.getPrezime()));
            brojTelefona.appendChild(dokument.createTextNode(unos.getBrojTelefona()));
            adresa.appendChild(dokument.createTextNode(unos.getAdresa()));
            nazivGrada.appendChild(dokument.createTextNode(unos.getNazivGrada()));
            element.appendChild(id);
            element.appendChild(ime);
            element.appendChild(prezime);
            element.appendChild(brojTelefona);
            element.appendChild(adresa);
            element.appendChild(nazivGrada);
        }
        //ovde proveravamo danasnji datum tako da svakog dana
        //pravimo novi dokument
        DateFormat formatDatuma = new SimpleDateFormat("dd,MM,yyyy");
        Date datum = new Date();

        String nazivFajla = (formatDatuma.format(datum) + ".xml");
        //pravimo tekstualni dokument
        File fajl = new File(nazivFajla);

        //ovde cuvamo xml u tekstualni dokument
        TransformerFactory fabrikaZaTransformaciju = TransformerFactory.newInstance();
        Transformer transformer = fabrikaZaTransformaciju.newTransformer();
        DOMSource izvorDOM = new DOMSource(dokument);
        StreamResult rezultat = new StreamResult(fajl);
        transformer.transform(izvorDOM, rezultat);
    }
}