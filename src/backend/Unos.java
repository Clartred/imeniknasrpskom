package backend;

public class Unos {

    private String ime, prezime, brojTelefona, adresa, nazivGrada;
    private int id;

    /**
     * konstruktor za pravljenje objekta unosa.
     */
    public Unos(int id, String ime, String prezime, String brojTelefona, String adresa, String nazivGrada) {
        this.id = id;
        this.ime = ime;
        this.prezime = prezime;
        this.brojTelefona = brojTelefona;
        this.nazivGrada = nazivGrada;
        this.adresa = adresa;
    }

    /**
     * konstruktor za pravljenje delimicnog unosa za potrebu baze podataka.
     */
    public Unos(int id, String ime, String prezime, String brojTelefona) {
        this.id = id;
        this.ime = ime;
        this.prezime = prezime;
        this.brojTelefona = brojTelefona;
    }

    /**
     * konstruktor za pravljenje delimicnog unosa za potrebu baze podataka.
     */
    public Unos(String adresa, String nazivGrada) {
        this.adresa = adresa;
        this.nazivGrada = nazivGrada;
    }


    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getBrojTelefona() {
        return brojTelefona;
    }

    public void setBrojTelefona(String brojTelefona) {
        this.brojTelefona = brojTelefona;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getNazivGrada() {
        return nazivGrada;
    }

    public void setNazivGrada(String nazivGrada) {
        this.nazivGrada = nazivGrada;
    }

    public int getId() {
        return id;
    }
}