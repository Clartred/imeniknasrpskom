package backend;

import frontend.Tabela;

import java.sql.SQLException;
import java.util.ArrayList;

public class Imenik {
    private BazaPodataka bazaPodataka;
    private ArrayList<Unos> lista;
    private Tabela tabela;

    /**
     * konstruktor za klasu Imenik
     */
    public Imenik(BazaPodataka bazaPodataka, ArrayList<Unos> lista, Tabela tabela) {
        this.bazaPodataka = bazaPodataka;
        this.lista = lista;
        this.tabela = tabela;
    }

    /**
     * metoda za dodavanje novih Unosa u ulancanu listu i zatim poziva metodu koje ce podatke uneti u bazu podataka.
     */
    public void dodajUnos(String ime, String prezime, String brojTelefona, String adresa, String nazivGrada) throws  SQLException  {
        ime = ime.toLowerCase();
        prezime = prezime.toLowerCase();
        brojTelefona = brojTelefona.toLowerCase();
        int idUnosa = bazaPodataka.unesiUBazu(ime, prezime, brojTelefona, nazivGrada, adresa);
        lista.add(new Unos(idUnosa, ime, prezime, brojTelefona, adresa, nazivGrada));
        tabela.uzmiUnose();
    }

    /**
     * Ova metoda brise unos iz ulancane liste i zatim poziva metodu koje ce izbrisati
     * unos iz baze podataka.
     * @param id unosa koji treba da se izbrise.
     */
    public void izbrisiUnos(int id) {
        for (int i = 0; i < lista.size(); i++) {
            Unos unos = lista.get(i);
            int idUnosa = unos.getId();
            if (idUnosa == id) {
                lista.remove(unos);
                try {
                    bazaPodataka.izbrisiUnos(idUnosa);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        }
        tabela.uzmiUnose();
    }


    /**
     * Ova metoda menja unos u ulancanoj listi a zatim poziva metodu
     * koje ce promeniti unos u bazi podataka.
     */
    public void promeniUnos(int id, String ime, String prezime, String brojTelefona, String adresa, String nazivGrada) {
        for (int i = 0; i < lista.size(); i++) {
            Unos unos = lista.get(i);
            if (unos.getId() == id) {
                unos.setIme(ime);
                unos.setPrezime(prezime);
                unos.setBrojTelefona(brojTelefona);
                unos.setAdresa(adresa);
                unos.setNazivGrada(nazivGrada);
                try {
                    bazaPodataka.prepraviUnos(unos);
                    tabela.uzmiUnose();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                    tabela.uzmiUnose();
                }
            }
        }
    }

    public ArrayList<Unos> getLista() {
        return lista;
    }
}