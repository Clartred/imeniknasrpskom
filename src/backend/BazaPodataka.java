package backend;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;

public class BazaPodataka {
    private static Connection konekcija;

    /**
     * Ova metoda nam omogucava da se konektujemo na bazu podataka.
     *
     * @return konekcija
     */
    public static Connection konektujSe() throws SQLException{
        if (konekcija == null) {
            String url = "jdbc:sqlite:Imenik.db";
            konekcija = DriverManager.getConnection(url);
            return konekcija;
        } else
            return konekcija;
    }

    /**
     * Ova metoda ce napraviti novu bazu podataka
     * i pozvati metodu koja ce napraviti novu tabelu u bazi podataka.
     */
    public void napraviNovuBazu() throws SQLException {
        String url = "jdbc:sqlite:Imenik.db";

        try (Connection konekcija = DriverManager.getConnection(url)) {
            if (konekcija != null) {
                DatabaseMetaData meta = konekcija.getMetaData();
                System.out.println(meta.getDriverName());
                napraviNovuTabelu();
            }
        }
    }

    /**
     * Ova metoda ce napraviti novu tabelu u bazi podataka
     */
    public void napraviNovuTabelu() throws  SQLException {
        Connection konekcija = konektujSe();

        String sqlSkripta = "Create table if not exists grad (idTabeleGrada integer primary key autoincrement," +
                " id_grada int, nazivGrada varchar(30), adresa varchar(30))";
        Statement sqlIzjava = konekcija.createStatement();
        sqlIzjava.execute(sqlSkripta);

        sqlSkripta = "Create table if not exists unos (idUnosa integer primary key autoincrement, ime varchar(30) , prezime varchar(30)," +
                " brojTelefona varchar(30),  idGrada int, FOREIGN  KEY (idGrada) REFERENCES grad (id_grada))";
        sqlIzjava = konekcija.createStatement();
        sqlIzjava.execute(sqlSkripta);
    }


    /**
     * Ova metoda ce uneti sve parametre u bazu podataka.
     *
     * @param ime
     * @param prezime
     * @param brojTelefona
     * @param adresa
     * @param nazivGrada
     * @return id unosa.
     */
    public int unesiUBazu(String ime, String prezime, String brojTelefona, String adresa, String nazivGrada) throws SQLException {
        int idGrada = 0;
        nazivGrada = nazivGrada.toLowerCase();
        switch (nazivGrada) {
            case "beograd":
                idGrada = 1;
                break;
            case "nis":
                idGrada = 1;
                break;
            case "novi sad":
                idGrada = 2;
                break;
            case "kragujevac":
                idGrada = 3;
                break;
            case "subotica":
                idGrada = 4;
                break;
            case "krusevac":
                idGrada = 5;
                break;
            case "kraljevo":
                idGrada = 6;
                break;
            case "pancevo":
                idGrada = 7;
                break;
            case "smederevo":
                idGrada = 8;
                break;
            case "zrenjanin":
                idGrada = 9;
                break;
            default:
                idGrada = 0;
        }

        Connection konekcija = this.konektujSe();
        String sqlSkripta = "INSERT INTO unos(idUnosa, ime, prezime, brojTelefona, idGrada) VALUES(?,?,?,?,?)";
        PreparedStatement sqlIzjava = konekcija.prepareStatement(sqlSkripta);

        sqlIzjava.setString(2, ime);
        sqlIzjava.setString(3, prezime);
        sqlIzjava.setString(4, brojTelefona);
        sqlIzjava.setInt(5, idGrada);
        sqlIzjava.executeUpdate();

        sqlSkripta = "INSERT INTO grad (idTabeleGrada, id_grada, adresa, nazivGrada) VALUES(?,?,?,?)";
        sqlIzjava = konekcija.prepareStatement(sqlSkripta);
        sqlIzjava.setInt(2, idGrada);
        sqlIzjava.setString(3, adresa);
        sqlIzjava.setString(4, nazivGrada);
        sqlIzjava.executeUpdate();

        String retrieveID = "SELECT idUnosa from unos where idUnosa =(SELECT max(idUnosa) from unos)";
        Statement izjava = konekcija.createStatement();
        ResultSet skupRezultata = izjava.executeQuery(retrieveID);
        return skupRezultata.getInt("idUnosa");
    }

    /**
     * Ova metoda proverava da li postoji baza podataka i
     * ukoliko ne postoji, poziva metodu koja ce je napraviti.
     */
    public void proveriDaLiBazaPostoji() {
        String putanja = "Imenik.db";
        File fajl = new File(putanja);
        if (!fajl.exists()) {
            try {
                napraviNovuBazu();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            try {
                this.konektujSe();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return;
        }
    }

    /**
     * Ova metoda omogucava da promenimo neki unos u bazi podataka.
     * @param unos koji treba da se promeni.
     */
    public void prepraviUnos(Unos unos) throws SQLException {

        String prepravi = "UPDATE unos set ime = ?, prezime = ?, brojTelefona = ?  WHERE idUnosa = '" + unos.getId() + "'";
        PreparedStatement SQLIzjava = konekcija.prepareStatement(prepravi);
        SQLIzjava.setString(1, unos.getIme());
        SQLIzjava.setString(2, unos.getPrezime());
        SQLIzjava.setString(3, unos.getBrojTelefona());
        SQLIzjava.executeUpdate();
        prepravi = "UPDATE grad set adresa = ?, nazivGrada = ?  WHERE idTabeleGrada = '" + unos.getId() + "'";
        PreparedStatement izjava = konekcija.prepareStatement(prepravi);
        izjava.setString(1, unos.getBrojTelefona());
        izjava.setString(2, unos.getNazivGrada());
        izjava.executeUpdate();
    }

    /**
     * Ova metoda brise unos u bazi podataka.
     * @param id unosa koji treba da bude obrisan.
     */
    public void izbrisiUnos(int id) throws SQLException {

        String skriptaZaBrisanjeUnosa = "DELETE FROM unos where idUnosa = '" + id + "'";
        String skriptaZaBrisanjeGrada = "Delete from grad where idTabeleGrada = '" + id + "'";
        PreparedStatement izjava = konekcija.prepareStatement(skriptaZaBrisanjeUnosa);
        izjava.executeUpdate();
        izjava = konekcija.prepareStatement(skriptaZaBrisanjeGrada);
        izjava.executeUpdate();
    }

    /**
     * Ova metoda povlaci sve unoose iz baze podataka.
     * @return ObservableList<Unos> unos.
     */
    public ObservableList<Unos> vratiUnose() {
        ArrayList<Unos> prvaLista = new ArrayList<>(), drugaLista = new ArrayList<>();
        String nadjiUnos = "SELECT * FROM unos";
        String nadjiGrad = "SELECT * FROM grad";

        try (
                Statement izjava = konekcija.createStatement();
                ResultSet skupRezultata = izjava.executeQuery(nadjiUnos)) {

            while (skupRezultata.next()) {
                prvaLista.add(new Unos(skupRezultata.getInt("idUnosa"), skupRezultata.getString("ime"),
                        skupRezultata.getString("prezime"), skupRezultata.getString("brojTelefona")));
            }
            try (Statement sqlIzjava = konekcija.createStatement();
                 ResultSet skupRezultataGrada = sqlIzjava.executeQuery(nadjiGrad)) {

                while (skupRezultataGrada.next()) {
                    drugaLista.add(new Unos(skupRezultataGrada.getString("adresa"), skupRezultataGrada.getString("nazivGrada")));
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        ObservableList<Unos> unos = FXCollections.observableArrayList();

        for (int i = 0; i < prvaLista.size(); i++) {
            Unos e = prvaLista.get(i);
            Unos x = drugaLista.get(i);
            unos.add(new Unos(e.getId(), e.getIme(), e.getPrezime(), e.getBrojTelefona(), x.getAdresa(), x.getNazivGrada()));
        }
        return unos;
    }
}