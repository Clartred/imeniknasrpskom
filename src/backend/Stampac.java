package backend;

import java.awt.*;
import java.awt.event.*;
import java.awt.print.*;
import java.util.ArrayList;

public class Stampac implements Printable, ActionListener {

    ArrayList<Unos> lista;

    public Stampac(ArrayList<Unos> lista) {
        this.lista = lista;
    }

    /**
     * ova metoda vadi podatke iz array liste i priprema ih
     * za stampanje na papir( u vidu pdf formata)
     */
    public int print(Graphics g, PageFormat pf, int page)  {

        if (page > 0) {
            return NO_SUCH_PAGE;
        }

        Graphics2D grafika = (Graphics2D) g;
        grafika.translate(pf.getImageableX(), pf.getImageableY());

        /* Now we perform our rendering */
        g.drawString("ime", 50, 20);
        g.drawString("prezime", 150, 20);
        g.drawString("broj telefona", 275, 20);
        g.drawString("adresa", 400, 20);
        g.drawString("grad", 500, 20);

        for (int i = 0, y = 50; i < lista.size(); i++, y = y + 20) {
            Unos e = lista.get(i);
            g.drawString(e.getIme(), 50, y);
            g.drawString(e.getPrezime(), 150, y);
            g.drawString(e.getBrojTelefona(), 275, y);
            g.drawString(e.getAdresa(),400, y);
            g.drawString(e.getNazivGrada(), 500, y);
        }

        return PAGE_EXISTS;
    }

    public void actionPerformed() {
        PrinterJob posao = PrinterJob.getPrinterJob();
        posao.setPrintable(this);
        boolean ok = posao.printDialog();
        if (ok) {
            try {
                posao.print();
            } catch (PrinterException ex) {
              //u slucaju da se nije istampalo,
                //dobijamo exception
            }
        }
    }

    /**
     * metoda koja se mora koristiti zbog toga sto smo nasledili interfejs
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {}
}